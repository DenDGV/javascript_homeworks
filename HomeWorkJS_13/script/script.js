let head = document.head;
let link = document.createElement('link');
link.rel = 'stylesheet';

if (localStorage.getItem('style') === 'Style2') {
    link.href = 'Style/Style2.css';
} else {
    link.href = 'Style/style.css';
}

head.appendChild(link);

document.getElementById('changeColor').addEventListener('click', function () {
    if (localStorage.getItem('style') === 'style') {
        link.href = 'Style/Style2.css';
        localStorage.setItem('style', 'Style2');
    } else {
        link.href = 'Style/style.css';
        localStorage.setItem('style', 'style');
    }
});