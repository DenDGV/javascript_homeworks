'use strict';


function createNewUser(firstName, secondName, birthDay) {
    let newUser = {
        firstName: firstName,
        secondName: secondName,
        birthday: birthDay,

        getAge : (function () {
            let year = Number(this.birthday.substr(6, 4));
            let month = Number(this.birthday.substr(3, 2)) - 1;
            let day = Number(this.birthday.substr(0, 2));
            let today = new Date();
            let age = today.getFullYear() - year;
            if (today.getMonth() < month || (today.getMonth() === month && today.getDate() < day)) {
                age--;
            }
            return age
        }),
    getPassword :( function () {
        return this.firstName.toUpperCase().charAt(0) + this.secondName.toLowerCase() + this.birthday.substr(6, 4);
    })
};
return newUser;
}
   let name = prompt(`Your first name`);
   let sName= prompt(`Your second name`);
   let birthDay =prompt(`Enter your birthday date`, `dd.mm.yyyy`);

  let User = createNewUser(name, sName, birthDay);


console.log(`New user: ${User['firstName']}` + " " + `${User['secondName']}`);
console.log(`Your birthday: ${User.birthday}`);
console.log(`Your age: ${User.getAge()}`);
console.log(`Your new password:${User.getPassword()}`);
