let head = document.head;
let link = document.createElement('link');
link.rel = 'stylesheet';

if (localStorage.getItem('style') === 'Style2') {
    link.href = 'Style/Style2.css';
} else {
    link.href = 'Style/style.css';
}

head.appendChild(link);

document.getElementById('changeColor').addEventListener('click', function () {
    if (localStorage.getItem('style') === 'style') {
        link.href = 'Style/Style2.css';
        localStorage.setItem('style', 'Style2');
    } else {
        link.href = 'Style/style.css';
        localStorage.setItem('style', 'style');
    }
});

$(document).ready(function(){
    $(".menuItems").on("click", function (event) {
        event.preventDefault();
        let id = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1500);
    });
});

$(document).ready(function () {
    $('#scrollUp').hide();
    $(window).scroll(function () {
        if ($(this).scrollTop()>$(window).height()) $('#scrollUp').show().addClass('animated flipInX');
        else $('#scrollUp').hide();
    });
    $('#scrollUp').click(function () {
        $('body, html').animate({
            scrollTop : 0
        }, 2000)
    })
});
$(document).ready(function () {
    $('#toggleBtn').click(function () {
        $('.postBox').slideToggle('slow')
    })
});
