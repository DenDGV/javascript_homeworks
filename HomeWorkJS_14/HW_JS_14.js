$(document).ready(function () {
    $(`.tabs-title`).click(function (){
        switchTabs (this ,`active` , `content`);
    });
    function switchTabs(elem ,switchClass , contentClass) {
        $(elem).addClass(switchClass).siblings().removeClass(switchClass);
        const tabIndex = $(elem).index();
        $(`.${contentClass}`).removeClass(switchClass).eq(tabIndex).addClass(switchClass);
    }
});