'use strict';
  let slides =document.querySelectorAll('.images-wrapper .image-to-show');
  let theSlide = 0;
  let intervalShow = setInterval(slideShow, 1000);
  let play = true;
  let stopBtn = document.getElementById('btnStop');
  let playBtn = document.getElementById('btnPlay');

function slideShow() {
    slides[theSlide].className = 'image-to-show';
    theSlide = (theSlide+1)%slides.length;
    slides[theSlide].className ='image-to-show show'
}
let show = function(){
    if (play) {
        play =false;
        clearInterval(intervalShow)
    } else if (!play) {
        play =true;
        intervalShow = setInterval(slideShow, 1000)
    }
};
stopBtn.onclick=function () {
    if(play){
        this.nextElementSibling.classList.remove('active');
        this.classList.add('active');
        show()
    }
};
playBtn.onclick=function () {
    if(!play){
        this.classList.add('active');
        this.previousElementSibling.classList.remove('active');
        show()
    }
};
// function startTimer(duration, display) {
//     let timer = duration, seconds;
//     setInterval(function () {
//         seconds = parseInt(timer % 10);
//         display.textContent = "Change after " + ":" + seconds;
//         if (--timer < 0) {
//             timer = duration;
//         }
//     }, 950);
// }
//
// window.onload = function () {
//     let tenSeconds = 60 * 5,
//         display = document.querySelector('#time');
//     startTimer(tenSeconds, display);
// };