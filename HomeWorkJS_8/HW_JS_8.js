// При загрузке страницы показать пользователю поле ввода (input) с надписью Price.
// Это поле будет служить для ввода числовых значений
// Поведение поля должно быть следующим:
//
//     При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При потере фокуса она пропадает.
//     Когда убран фокус с поля - его значение считывается, над полем создается span, в котором должен быть выведен
//     текст: Текущая цена: ${значение из поля ввода}. Рядом с ним должна быть кнопка с крестиком (X).
//     Значение внутри поля ввода окрашивается в зеленый цвет.
//     При нажатии на Х - span с текстом и кнопка X должны быть удалены.
//     Значение, введенное в поле ввода, обнуляется.
//     Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой,
//     под полем выводить фразу - Please enter correct price. span со значением при этом не создается.
//

let closeBtnResult = document.getElementById('closeResult');
let resultWindow = document.getElementById('resPriceWindow');
let inputPrice = document.getElementById('inputPrice');

    closeBtnResult.onclick = function () {
    resultWindow.style.display = 'none';
    inputPrice.value = '';
};
     inputPrice.onfocus = function () {
        this.classList.add('inFocus')
    };
     inputPrice.onblur = function() {
        this.classList.remove('inFocus');
        this.classList.add('textOutFocus');
         if (this.value > 0) {
             this.classList.remove('errorValue');
             resultWindow.style.display = 'block';
             let result = this.value;
             console.log(result);
             document.getElementById('resPrice').innerHTML = `ACURA NSX ` + `${result}` + `$`;
             let error = document.getElementById('mistake');
             error.innerText = '';
         }
         else if (this.value <= 0) {
             this.classList.remove('textOutFocus');
             this.classList.add('errorValue');
             resultWindow.style.display = 'none';
             let error = document.getElementById('mistake');
             error.innerHTML = 'Enter the correct price please!'
         }
    };

